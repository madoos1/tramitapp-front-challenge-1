import FixedHeader from './components/FixedHeader'
import Item from './components/Item'
import useScrollTop from './hooks/useScrollTop'

const App = () => {
  const items = Array.from({ length: 25 })
  const scrollInTop = useScrollTop(50)

  return (
    <div className="App">
      {scrollInTop && <FixedHeader />}
      {items.map((_, i) => <Item key={i} />)}
    </div>
  );
}

export default App;
