import { useEffect, useState } from 'react'

const useScrollTop = (threshold = 0) => {
  const [scrollInTop, setIsInTop] = useState(true)

  useEffect(() => {
    const handleState = () => (window.pageYOffset <= threshold) ? setIsInTop(true) : setIsInTop(false)
    window.addEventListener('scroll', handleState)
    return () => window.removeEventListener('scroll', handleState)
  }, [])

  return scrollInTop
}

export default useScrollTop