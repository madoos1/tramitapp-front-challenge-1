# TramitApp Front-End challenge #1

Realizar una página con una cabecera que se esconda al hacer scroll hacia abajo en una lista, pero que en cuanto empieces a hacer scroll hacia arriba, vuelva a aparecer.

# Iniciar APP

* npm install
* npm start

## Solución

Para implementar este ejercicio he creado un custom hook `useScrollTop` que me dice si el scroll está en el top, esta señal será usada por el componente App (mediador) para decidir si se muestra el componente FixedHeader.

```js
import FixedHeader from './components/FixedHeader'
import Item from './components/Item'
import useScrollTop from './hooks/useScrollTop'

const App = () => {
  const items = Array.from({ length: 25 })
  const scrollInTop = useScrollTop(50)

  return (
    <div className="App">
      {scrollInTop && <FixedHeader />}
      {items.map((_, i) => <Item key={i} />)}
    </div>
  );
}
```
## Ejemplo:

![](example.gif)